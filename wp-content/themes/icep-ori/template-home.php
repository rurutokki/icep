<?php
/* Template Name: Home */
?>

<?php get_header(); ?>

<?php get_template_part('/inc/main-nav'); ?>

<div class="site-wrap">
    <div class="content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; else: ?>
            There is no article.
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>

