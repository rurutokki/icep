<?php
/* Template Name: About us - Our Leader */ 
get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				//get_template_part( 'template-parts/page/content', 'page' );
			
			?>
			<div class="page-content">
				<section>
					
						<?php
							the_content();
							echo do_shortcode('[tmm name="muhammad-zamri-jusoh"]');
							echo do_shortcode('[tmm name="director-1"]');
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
								'after'  => '</div>',
							) );
						?>
					
				</section>
				<section>
					<h1>Alternate Board of Directors</h1>
						<?php
					
							
							echo do_shortcode('[tmm name="director-1"]');
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
								'after'  => '</div>',
							) );
						?>
					
				</section>
				<section>
					<h1>Management Team</h1>
						<?php
					
							echo do_shortcode('[tmm name="muhammad-zamri-jusoh"]');
							echo do_shortcode('[tmm name="director-1"]');
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
								'after'  => '</div>',
							) );
						?>
					
				</section>
				
				
				
			</div>	
			<?php	
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->



<?php get_footer();
