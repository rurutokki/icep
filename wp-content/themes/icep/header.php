<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/custom.css">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
		<div class="bigmenu">
		<?php 
		echo '<nav class="">';
		wp_nav_menu( array(
		'theme_location' => 'mainmenu',
		'menu_id'        => 'mainmenu',
		'menu_class'     => 'mainmenu',     
		) ); 
		echo '</nav>'; 
		?>
		</div> 
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<?php get_template_part( 'template-parts/header/header', 'icep' ); ?>

	

	</header><!-- #masthead -->

	<?php
	
	$current = $post->ID;
	$parent = $post->post_parent;
	$grandparent_get = get_post($parent);
	$grandparent = $grandparent_get->post_parent;
	if ($root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current)) {
		$page_title =  get_the_title($grandparent); 
	}else {
		$page_title =  get_the_title($parent); 
	}
	$header_img_url = wp_get_attachment_url( get_post_thumbnail_id($parent) );
	$slug =  get_the_slug($parent);
	if($grandparent != 0){
		$header_img_url = wp_get_attachment_url( get_post_thumbnail_id($grandparent) );
		$slug =  get_the_slug($grandparent);
	}
	
	
	// If a regular post or page, and not the front page, show the featured image.
	if ( has_post_thumbnail() && ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) ){
		echo '<div class="single-featured-image-header" style="background: url('.$header_img_url.') no-repeat center top / cover">';
	
		echo '<div class="container">';
		echo '<nav class="pagenav ">';
			 wp_nav_menu( array(
				'theme_location' => $slug,
				'menu_id'        => 'page-menu',
				'menu_class'     => 'nav navbar-nav',     
			) ); 
		echo '</nav>';
		echo '<div class="page-title-containerxxx"><div class="page-title"><h2>'.$page_title.'</h2></div></div>';
		echo '</div>';
		echo '</div><!-- .single-featured-image-header -->';
		}
	/*else if ($grandparent != 0 ){
		
		echo '<div class="single-featured-image-header" style="">';
			echo '<div class="container">';
			 echo '<nav class="pagenav ">';
			 wp_nav_menu( array(
				'theme_location' => $slug,
				'menu_id'        => 'page-menu',
				'menu_class'     => 'nav navbar-nav',     
			) ); 
		echo '</nav>';
		



	
	
		echo '<div class="page-title-containerxxx"><div class="page-title"><h2>'.$page_title.'</h2></div></div>';
			
		echo '</div>';
		echo '</div>';
	}*/
	else if (twentyseventeen_is_frontpage() ){
		
		
	}
	else{
		echo '<div class="single-featured-image-header" style="background:url('.$header_img_url.') no-repeat center top / cover">';
			echo '<div class="container">';
			 echo '<nav class="pagenav ">';
			 wp_nav_menu( array(
				'theme_location' => $slug,
				'menu_id'        => 'page-menu',
				'menu_class'     => 'nav navbar-nav',     
			) ); 
		echo '</nav>';
		



	
	
		echo '<div class="page-title-containerxxx"><div class="page-title"><h2>'.$page_title.'</h2></div></div>';
			
		echo '</div>';
		echo '</div>';
}
	?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
			<button class="hamburger">&#9776;</button>
			<button class="cross">&#735;</button>
			
