<?php
/**
 * The template for displaying the footer
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container">
			<div class="row">
				<div class="col-md-6">
				<?php

				wp_nav_menu( array(
					'theme_location' => 'footer1',
					'menu_id'        => 'footer-menu',
					'menu_class'        => 'footer-menu',
					'items_wrap'      => '<ul  id="footer-menu" class="footer-menu"><li id="item-id"><strong>Home</strong></li>%3$s</ul>'
					  
				) );
				
				wp_nav_menu( array(
					'theme_location' => 'footer2',
					'menu_id'        => 'footer-menu2',
					'menu_class'        => 'footer-menu',
					'items_wrap'      => '<ul id="footer-menu2" class="footer-menu" ><li id="item-id"><strong>Conferences & Events</strong></li>%3$s</ul>' 
				) );
				
				wp_nav_menu( array(
					'theme_location' => 'footer3',
					'menu_id'        => 'footer-menu3',
					'menu_class'        => 'footer-menu',
					'items_wrap'      => '<ul id="footer-menu3" class="footer-menu"><li id="item-id"><strong>Press Room</strong></li>%3$s</ul>' 
				) );
				
				wp_nav_menu( array(
					'theme_location' => 'footer4',
					'menu_id'        => 'footer-menu4',
					'menu_class'        => 'footer-menu',
					'items_wrap'      => '<ul id="footer-menu4" class="footer-menu"><li id="item-id"><strong>Talk to Us</strong></li>%3$s</ul>' 
				) );
				
			
				

				
				?>
				</div>
				<div class="col-md-6">
					<?php 
					get_template_part( 'template-parts/footer/footer', 'widgets' );

					if ( has_nav_menu( 'social' ) ) : ?>
					<nav class="social-navigation" role="navigation" aria-label="<?php _e( 'Footer Social Links Menu', 'icep' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span class="screen-reader-text">',
								'link_after'     => '</span>' . twentyseventeen_get_svg( array( 'icon' => 'chain' ) ),
							) );
						?>
					</nav><!-- .social-navigation -->
				<?php endif; ?>
				</div>
				<?php get_template_part( 'template-parts/footer/site', 'info' ); ?>
			</div><!-- .row -->
			</div><!-- .container -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
$( ".cross" ).hide();
$( ".bigmenu" ).hide();
$( ".hamburger" ).click(function() {
$( ".bigmenu" ).fadeToggle( "fast", function() {
$( ".hamburger" ).hide();
$( ".cross" ).fadeIn();
});
});

$( ".cross" ).click(function() {
$( ".bigmenu" ).fadeToggle( "fast", function() {
$( ".cross" ).hide();
$( ".hamburger" ).fadeIn();
});
});
<?php if(twentyseventeen_is_frontpage()){ ?>
(function($) {          
    $(document).ready(function(){                    
        $(window).scroll(function(){                          
            if ($(this).scrollTop() > 100) {
                //$('.icep-navbar').fadeIn(500);
                $('.icep-navbar').addClass('active');
                $('#top-menu').show();
            } else {
                //$('.icep-navbar').fadeOut(500);
				 $('.icep-navbar').removeClass('active');
				 $('#top-menu').hide();
            }
        });
    });
})(jQuery);
<?php }else{ ?>
$('#top-menu').show();
/*
(function($) {          
    $(document).ready(function(){                    
        $(window).scroll(function(){                          
            if ($(this).scrollTop() > 77) {
				//$(".icep-navbar").slideUp("", function() {
					$(".icep-navbar").addClass("active_page");
				//});
               
             
            } else {
				// $(".icep-navbar").slideDown("", function() {
					$(".icep-navbar").removeClass("active_page");
				//});
            }
        });
    });
})(jQuery);*/
<?php } ?>
</script>
</body>
</html>
