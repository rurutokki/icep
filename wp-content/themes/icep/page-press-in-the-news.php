<?php
/* Template Name: Press Room - In the news */ 
get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				//get_template_part( 'template-parts/page/content', 'page' );

			?>
			<div class="page-content">
				<section>
					<div class="container">
						<?php
							the_content();
							
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
								'after'  => '</div>',
							) );
						?>
					</div>
				</section>
				<br/>
				<br/>
				<section>
					<div class="col-md-12">
					<div class="row greybox">
						<div class="col-md-7">
							<div class="row">
								<div class="featuredimage">
								<?php the_post_thumbnail('full', array( 'class' => 'w100' ));?>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="visionmission">
								<img src="<?php bloginfo('template_url'); ?>/assets/images/vision.png">
								<p></p>
								<p><?php the_field('vision'); ?></p>
								<div class="space"></div>
								<div class="space"></div>
								<img src="<?php bloginfo('template_url'); ?>/assets/images/mission.png">
								<p><?php the_field('mission'); ?></p>
							</div>
						</div>
					</div>
					</div>
				</section>
				<section>
					<div class="container">
						<h1>Our Trophy Case</h1>
					</div>
				<div class="col-md-12">
					<div class="row greybox">
					<?php

					$post_object = get_field('add_article');

					if( $post_object ): 

						// override $post
						$post = $post_object;
						setup_postdata( $post ); 
						
						?>
						
						
						<div class="col-md-8">
							<div class="trophy">
								<h2><?php the_title(); ?></h2>
								<?php the_content(); ?>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="featuredimage text-right">	
									<?php the_post_thumbnail();?>
								</div>
							</div>
						</div>
					<?php wp_reset_postdata(); 
					endif; ?>
					</div>
					</div>
					
				</section>
				<section>
					<div class="container">
						<h1>Our Services</h1>
					</div>
				</section>
			</div>	
			<?php	
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->



<?php get_footer();
