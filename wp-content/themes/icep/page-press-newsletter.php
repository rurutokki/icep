<?php
/* Template Name: Press Room - Newsletter */ 
get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				//get_template_part( 'template-parts/page/content', 'page' );

			?>
			<div class="page-content">
				<section>
				<!--<ul class="list-unstyled">
				<?php
				
				$mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'asc' ) );

				foreach( $mypages as $page ) {		
					$content = $page->post_content;
					//if ( ! $content ) // Check for empty page
					//	continue;

					$content = apply_filters( 'the_content', $content );
				?>
					<li>
						<div class="col-md-6">
							<div class="row">
								<div class="nlbox-container">
									<a href="<?php echo get_page_link( $page->ID ); ?>">
										<div class="nlbox boxleft">
											<img src="<?= wp_get_attachment_url( get_post_thumbnail_id($page->ID) ); ?>">
										</div>
										<div class="nlbox boxright">
											<h2><?php echo $page->post_title; ?> -<br><span><?= get_the_date( "F Y", $page->ID ); ?></span></h2>
										</div>
										<div class="orangeoverlay">
											<div class="nlbox boxright" style="float: right">
											<h2><?php echo $page->post_title; ?> -<br><span><?= get_the_date( "F Y", $page->ID ); ?></span></h2>
										</div>
										</div>
									</a>
									
									
								</div>
							</div>
						</div>
					</li>
					
				<?php
				}	
			?>
				</ul>-->
					<div class="container">
						<?php
							the_content();
							//echo '<h1>'.get_the_title().'</h1>';
							wp_link_pages( array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
								'after'  => '</div>',
							) );
						?>
						
					</div>
					
					<?php

					echo do_shortcode('[ajax_load_more  posts_per_page="2" post_type="page" category ="newsletter" button_label="View more"]');
					?>
				</section>
				
			
				
			</div>	
			<?php	
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->



<?php get_footer();
