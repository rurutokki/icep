<?php
/**
 * Template part for displaying page content in page.php

 */

?>
<div class="page-content">
	<div class="container">
		<?php
			the_content();
			
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'  => '</div>',
			) );
		?>
	</div>
	<br/>
	<br/>

</div>

