<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'icep');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wO<%[p;wrPeN&zK,`Z|!d{/H}i^}cD*vPIn{j9%27m mkj`Yd*a>jHJKb#oa~@wP');
define('SECURE_AUTH_KEY',  '_T~mzqH}#:Bdl TTy/9mYhTGY{kCl-g^dJ@3!m@ 2@lOnd0Kw5;ar wW#MUIf}Z]');
define('LOGGED_IN_KEY',    'b%3F?S$@sxcTQB!VZZhhUr2k.^=:fHikhyH}C ]RAKO3qNfQxd{gPkBuY(p,Ga$I');
define('NONCE_KEY',        'q^;(aL!SSAv>4q Cr#ol=B1.L1qMf>;zI7sF|_)YpljQyWEqV&L8_sl_CB[]KFtw');
define('AUTH_SALT',        'j}HKL+@h}&`&l1eg_No,XBm}d?A&4G{A9|;]$kmD86;qYsfK$%F<o(j-qAmfHW`8');
define('SECURE_AUTH_SALT', 'o:w7Fb`[-m+xb*Fe)p8A]|h vuJ}SkXqSS%B+I;he&m?_H19UYRN{I0(tPd?5Ud_');
define('LOGGED_IN_SALT',   '=Y>cTBplp{E2jhGuJul5Az,(}7pQHlDD9,<|05x3l[R3Lvczjz2aoCwOj2kN VRV');
define('NONCE_SALT',       '5t<=[} r-`Gi@SS+xma@y6cP&&*wI]yB]Y_OZw0B/e#$cu]Sc! &A!HD|!9dFnvH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_POST_REVISIONS', false );
define ('EMPTY_TRASH_DAYS', 0);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
